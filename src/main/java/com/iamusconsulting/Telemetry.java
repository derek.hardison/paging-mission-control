package com.iamusconsulting;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class Telemetry {
    public static final String THERMOSTAT = "TSTAT";
    public static final String BATTERY = "BATT";

    public Telemetry next;
    public int id;
    public LocalDateTime timestamp;
    public String component;
    public Double low;
    public Double high;
    public Double value;
    public Telemetry(int id, String component, LocalDateTime timestamp, Double high, Double low, Double value) {
        this.id = id;
        this.component = component;
        this.timestamp = timestamp;
        this.high = high;
        this.low = low;
        this.value = value;
    }

    public boolean isAlertCondition() {
        return (this.component.equalsIgnoreCase(BATTERY) && this.value < this.low) ||
                (this.component.equalsIgnoreCase(THERMOSTAT) && this.value > this.high);
    }

    public boolean isWithinTimeLimit(Telemetry e2) {
        return e2 == null || Math.abs(timestamp.until(e2.timestamp, ChronoUnit.MINUTES)) < 5;
    }

    public boolean isBattery() {
        return this.component.equalsIgnoreCase(BATTERY);
    }
}
