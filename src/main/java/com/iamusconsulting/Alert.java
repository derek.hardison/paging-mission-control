package com.iamusconsulting;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

public class Alert {
    public int satelliteId;
    public String severity;
    public String component;
    public LocalDateTime timestamp;

    public Alert(int satelliteId, String severity, String component, LocalDateTime timestamp) {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp;
    }
}
