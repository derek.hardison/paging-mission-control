package com.iamusconsulting;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import picocli.CommandLine;

import java.io.File;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Stream;

@CommandLine.Command(name = "Pager Mission Control", description = "Generates alert messages based on telemetry from input file.")
public class Main implements Callable<Integer> {
    public static final String TIMESTAMP_FORMAT = "yyyyMMdd HH:mm:ss.SSS";
    public static final int ALERT_THRESHOLD = 3;

    private final HashMap<String, Telemetry> head = new HashMap<>();
    private final HashMap<String, Telemetry> tail = new HashMap<>();
    private final HashMap<String, Integer> counter = new HashMap<>();

    private final List<Alert> alerts = new ArrayList<>();

    @CommandLine.Parameters(index = "0", description = "Path to the input telemetry file.")
    private String fileName;

    public static void main(String[] args) {
        int code = new CommandLine(new Main()).execute(args);
        System.exit(code);
    }

    @Override
    public Integer call() throws Exception {
        try (Stream<String> stream = Files.lines(new File(fileName).toPath())) {
            // read the input file and process lines into Telemetry object before
            // checking for alarms.
            stream.map(this::processLine)
                    .forEach(this::processTelemetry);
        }

        // write alerts using Jackson JSON serializer to console.
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        System.out.println(mapper.writeValueAsString(alerts));

        return 0;
    }

    private String getKey(Telemetry e) {
        return e.id +":"+ e.component;
    }

    private void processTelemetry(Telemetry e1) {
        final String keyId = getKey(e1);

        // sliding window (e1 -> right boundary (latest telemetry data), e2 -> left boundary)
        Telemetry e2 = head.get(keyId);

        if (tail.containsKey(keyId)) {
            // append new element to the linked list and update
            // the tail pointer to point to the latest entry.
            tail.get(keyId).next = e1;
            tail.put(keyId, e1);
        }

        // pointer to the left (head) & right (tail) boundary of the sliding window
        head.putIfAbsent(keyId, e1);
        tail.putIfAbsent(keyId, e1);

        while (e2 != null && (!e1.isWithinTimeLimit(e2) || !e2.isAlertCondition())) {
            // slide window to the right since we are no longer within the
            // allowed timeframe (5 minutes) and decrement the number of alert events seen
            head.put(getKey(e2), e2.next);
            e2 = e2.next;
            counter.compute(keyId, (key, value) -> value == null || value <= 0 ? 0 : value - 1);
        }

        if (e1.isWithinTimeLimit(e2) && e1.isAlertCondition()) {
            // still within timeframe and we are an alert condition
            // increment the counter below.
            counter.compute(keyId, (key, value) -> value == null ? 1 : value + 1);

            int c = counter.get(keyId);
            if (c % ALERT_THRESHOLD == 0 && c > 0) {
                // alert threshold met, append to alert list
                // use left boundary timestamp when creating alert.
                alerts.add(
                        new Alert(
                                e1.id,
                                e1.isBattery() ? "RED LOW" : "RED HIGH",
                                e1.component,
                                e2 == null ? LocalDateTime.now() : e2.timestamp
                        )
                );
            }
        }
    }

    private Telemetry processLine(String line) {
        // split each line on the pipe "|" delimiter.
        // <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
        String[] data = line.split("\\|");

        // parse timestamps using format specified at top of file.
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(TIMESTAMP_FORMAT);
        LocalDateTime date = LocalDateTime.parse(data[0], formatter);

        return new Telemetry(
                Integer.parseInt(data[1]),
                data[7],
                date,
                Double.parseDouble(data[2]),
                Double.parseDouble(data[5]),
                Double.parseDouble(data[6])
        );
    }
}